FROM openjdk:17-jdk-alpine
COPY app/build/libs/java-lab-app-1.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
