package ro.axonsoft.wsw.infra.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ContextConfiguration(classes = RoomDbConfig.class)
@DataJpaTest
class RoomRepositoryTest {

    @Autowired
    private RoomRepository repository;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    void getById_roomFound() {
        var roomEty = new RoomEty();
        roomEty.setId("1111");
        var person = new PersonEty();
        person.setFirstName("Frank");
        person.setLastName("Super");
        person.setId("dsfs");
        person.setNameAddition("van");
        person.setTitle("");
        roomEty.setPeople(List.of(person));
        repository.save(roomEty);
        entityManager.flush();
        assertThat(repository.findById("1111")).isNotNull();
        assertThat(repository.findById("1111").get()).satisfies(room -> {
            assertThat(room.getId()).isEqualTo("1111");
            assertThat(room.getPeople()).isNotNull();
            assertThat(room.getPeople().get(0).getTitle()).isEmpty();
        });
        assertThat(repository.findById("1111").get()).isNotSameAs(roomEty);
    }

    @Test
    void getById_roomNotFound() {
        var roomEty = new RoomEty();
        roomEty.setId("1111");
        var person = new PersonEty();
        person.setFirstName("Frank");
        person.setLastName("Super");
        person.setId("dsfs");
        person.setNameAddition("van");
        person.setTitle("");
        roomEty.setPeople(List.of(person));
        repository.save(roomEty);
        entityManager.flush();
        assertThat(repository.findById("1112")).isNotPresent();
    }

    @Test
    void findAll_returnsEmptyList() {
        assertThat(repository.findAll().isEmpty()).isTrue();
    }

    @Test
    void findAll_returnsList() {
        var roomEty = new RoomEty();
        roomEty.setId("1111");
        var person = new PersonEty();
        person.setFirstName("Frank");
        person.setLastName("Super");
        person.setId("dsfs");
        person.setNameAddition("van");
        person.setTitle("");
        roomEty.setPeople(List.of(person));
        repository.save(roomEty);
        entityManager.flush();
        assertThat(repository.findAll().isEmpty()).isFalse();
    }

}
