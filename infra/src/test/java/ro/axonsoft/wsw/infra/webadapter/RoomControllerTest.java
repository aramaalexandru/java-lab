package ro.axonsoft.wsw.infra.webadapter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import java.util.List;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ro.axonsoft.wsw.core.domain.PersonDetails;
import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.core.exception.InvalidRoomNumberException;
import ro.axonsoft.wsw.core.exception.RoomNotFoundException;
import ro.axonsoft.wsw.core.port.RoomDetailsServicePort;

import static org.mockito.Mockito.*;

@WebMvcTest(RoomController.class)
@ContextConfiguration(classes = {RoomMapperImpl.class, PersonMapperImpl.class, RoomExceptionHandler.class, RoomController.class})
class RoomControllerTest {

    public static final String INVALID_ROOM_NUMBER = "111111";
    public static final String ROOM_NOT_FOUND = "1999";
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RoomDetailsServicePort roomDetailsService;

    @SpyBean
    private RoomMapper roomMapper;

    @Test
    void whenFirstGetRoomsCall_thenReturnEmptyList() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/room"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(
                MockMvcResultMatchers.content().json("[]"));
        verify(roomDetailsService).getRooms();
    }

    @Test
    void whenGetRoomsCall_thenReturnRooms() throws Exception {
        var room1 = RoomDetails.builder()
                .roomNumber("1111")
                .build();
        var room2 = RoomDetails.builder()
                .roomNumber("1112")
                .persons(List.of(PersonDetails.builder()
                        .firstName("Frank")
                        .lastName("Super")
                        .ldapuser("dsfs")
                        .nameAddition("von")
                        .title("Dr.")
                        .build()))
                .build();
        doReturn(List.of(room1, room2)).when(roomDetailsService).getRooms();

        mockMvc.perform(MockMvcRequestBuilders.get("/api/room"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(
                MockMvcResultMatchers.jsonPath("$.size()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].room").value("1111"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].room").value("1112"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].people.size()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].people[0].ldapuser").value("dsfs"));
        verify(roomDetailsService).getRooms();
    }

    @Test
    void whenGetSpecificRoom_thenReturnRoom() throws Exception {
        var room = RoomDetails.builder()
                .roomNumber("1113")
                .build();
        doReturn(room).when(roomDetailsService).getRoomById("1113");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/room/{number}", "1113"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(
                MockMvcResultMatchers.jsonPath("$.room").value("1113"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.people").isEmpty());
        verify(roomDetailsService).getRoomById("1113");
    }

    @Test
    void whenGetInvalidRoom_thenReturnBadRequest() throws Exception {
        doThrow(new InvalidRoomNumberException()).when(roomDetailsService).getRoomById(INVALID_ROOM_NUMBER);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/room/{number}", INVALID_ROOM_NUMBER))
                .andExpect(MockMvcResultMatchers.status().isBadRequest()).andExpect(
                MockMvcResultMatchers.jsonPath("$.code").value("6"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").isNotEmpty());
        verify(roomDetailsService).getRoomById(INVALID_ROOM_NUMBER);
    }

    @Test
    void whenGetUnknownRoom_thenReturnNotFound() throws Exception {
        doThrow(new RoomNotFoundException()).when(roomDetailsService).getRoomById(ROOM_NOT_FOUND);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/room/{number}", ROOM_NOT_FOUND))
                .andExpect(MockMvcResultMatchers.status().isNotFound()).andExpect(
                MockMvcResultMatchers.jsonPath("$.code").value("5"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Room Not Found"));
        verify(roomDetailsService).getRoomById(ROOM_NOT_FOUND);
    }
}
