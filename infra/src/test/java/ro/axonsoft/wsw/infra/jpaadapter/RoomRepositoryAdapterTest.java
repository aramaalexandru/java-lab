package ro.axonsoft.wsw.infra.jpaadapter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.infra.jpa.RoomEty;
import ro.axonsoft.wsw.infra.jpa.RoomRepository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoomRepositoryAdapterTest {

    @Mock
    private RoomRepository roomRepository;

    @Spy
    private RoomJpaMapper roomMapper = new RoomJpaMapperImpl();

    @InjectMocks
    private RoomRepositoryAdapter subject;

    @Test
    void getRooms_returnsList(){
        var room = new RoomEty();
        room.setId("1111");
        room.setPeople(List.of());
        doReturn(List.of(room)).when(roomRepository).findAll();
        var result = subject.getRooms();
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(1);
        verify(roomRepository).findAll();
    }

    @Test
    void getRoomById_returnFoundRoom(){
        var room = new RoomEty();
        room.setId("1111");
        room.setPeople(List.of());
        doReturn(Optional.of(room)).when(roomRepository).findById("1111");
        var result = subject.getRoomById("1111");
        assertThat(result).isNotNull();
        assertThat(result).isPresent();
        assertThat(result.get().getRoomNumber()).isEqualTo("1111");
        assertThat(result.get().getPersons().size()).isEqualTo(0);
        verify(roomRepository).findById("1111");
    }

    @Test
    void saveAll_success(){
        var roomEty = new RoomEty();
        roomEty.setId("1111");
        roomEty.setPeople(List.of());
        var roomDetails = new RoomDetails();
        roomDetails.setRoomNumber("1111");
        roomDetails.setPersons(List.of());
        subject.saveAll(List.of(roomDetails));
        verify(roomRepository).saveAll(List.of(roomEty));
    }
}
