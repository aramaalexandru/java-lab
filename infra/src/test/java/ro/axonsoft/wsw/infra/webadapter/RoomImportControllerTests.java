package ro.axonsoft.wsw.infra.webadapter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ro.axonsoft.wsw.core.exception.WswErrorCode;
import ro.axonsoft.wsw.core.exception.WswException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RoomImportController.class, RoomExceptionHandler.class})
@WebMvcTest
class RoomImportControllerTests {

  @Autowired
  MockMvc mockMvc;

  @MockBean
  private ro.axonsoft.wsw.core.domain.port.RoomImportServicePort roomImportServicePort;

  static final MockMultipartFile MOCK_VALID_CSV_FILE = new MockMultipartFile("rooms",
      "sitzplan.csv", "text/csv", "".getBytes());
  static final MockMultipartFile MOCK_DUP_PERSON_CSV_FILE = new MockMultipartFile("rooms",
      "sitzplan_dupperson.csv", "text/csv", "".getBytes());

  static final MockMultipartFile MOCK_DUP_ROOM_CSV_FILE = new MockMultipartFile("rooms",
      "sitzplan_duproom.csv", "text/csv", "".getBytes());

  static final MockMultipartFile MOCK_INVALID_LINE_CSV_FILE = new MockMultipartFile("rooms",
      "sitzplan_invalid.csv", "text/csv", "".getBytes());

  @Test
  void performSuccessfulRoomsImport() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.multipart("/api/import")
            .file(MOCK_VALID_CSV_FILE)
            .contentType(MediaType.MULTIPART_FORM_DATA))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void performImportRooms_noBody_contentTypeSet_badRequest() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/api/import")
            .contentType(MediaType.MULTIPART_FORM_DATA))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
    verifyNoInteractions(roomImportServicePort);
  }

  @Test
  void performImportRooms_noBody_contentTypeAuto_badRequest() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/api/import")
            .contentType(MediaType.ALL))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
    verifyNoInteractions(roomImportServicePort);
  }

  @Test
  void performImportRooms_duplicateRoom_badRequest() throws Exception {
    doThrow(new WswException(WswErrorCode.DUPLICATE_ROOM)).when(roomImportServicePort).uploadFile(any());
    mockMvc.perform(MockMvcRequestBuilders.multipart("/api/import")
            .file(MOCK_DUP_ROOM_CSV_FILE)
            .contentType(MediaType.MULTIPART_FORM_DATA))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());

    verify(roomImportServicePort, times(1)).uploadFile(any());
  }

  @Test
  void performImportRooms_duplicatePerson_badRequest() throws Exception {
    doThrow(new WswException(WswErrorCode.DUPLICATE_PERSON)).when(roomImportServicePort).uploadFile(any());
    mockMvc.perform(MockMvcRequestBuilders.multipart("/api/import")
            .file(MOCK_DUP_PERSON_CSV_FILE)
            .contentType(MediaType.MULTIPART_FORM_DATA))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
    verify(roomImportServicePort, times(1)).uploadFile(any());
  }

  @Test
  void performImportRooms_invalidLine_badRequest() throws Exception {
    doThrow(new WswException(WswErrorCode.INVALID_LINE)).when(roomImportServicePort).uploadFile(any());

    mockMvc.perform(MockMvcRequestBuilders.multipart("/api/import")
            .file(MOCK_INVALID_LINE_CSV_FILE)
            .contentType(MediaType.MULTIPART_FORM_DATA))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());

    verify(roomImportServicePort, times(1)).uploadFile(any());
  }

}
