package ro.axonsoft.wsw.infra.webadapter;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ro.axonsoft.wsw.core.domain.port.RoomImportServicePort;

@RestController
@RequiredArgsConstructor
public class RoomImportController {

    private final RoomImportServicePort roomImportServicePort;


    @SneakyThrows
    @PostMapping(
            value = "/api/import",
            produces = { "application/json" },
            consumes = { "multipart/form-data" }
    )
    public ResponseEntity<Void> uploadFile(@RequestPart(value = "rooms") MultipartFile file) {
        roomImportServicePort.uploadFile(file.getInputStream());
        return ResponseEntity.ok().build();
    }

}

