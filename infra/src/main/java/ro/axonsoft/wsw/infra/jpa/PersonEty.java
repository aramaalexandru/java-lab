package ro.axonsoft.wsw.infra.jpa;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Embeddable
@Data
@EqualsAndHashCode(of = "id")
public class PersonEty {
    @Column(name = "LDAPUSER")
    public String id;
    @Column(name = "FIRSTNAME")
    public String firstName;
    @Column(name = "LASTNAME")
    public String lastName;
    @Column(name = "NAMEADDITION")
    public String nameAddition;
    @Column(name = "TITLE")
    public String title;
}
