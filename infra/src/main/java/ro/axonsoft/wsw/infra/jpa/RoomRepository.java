package ro.axonsoft.wsw.infra.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<RoomEty, String>{

}
