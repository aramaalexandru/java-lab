package ro.axonsoft.wsw.infra.webadapter;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ro.axonsoft.wsw.core.exception.InvalidRoomNumberException;
import ro.axonsoft.wsw.core.exception.RoomNotFoundException;
import ro.axonsoft.wsw.core.exception.WswErrorDetails;
import ro.axonsoft.wsw.core.port.RoomDetailsServicePort;
import ro.axonsoft.wsw.infra.webmodel.RoomDto;

@Slf4j
@RestController
@RequiredArgsConstructor
public class RoomController {

    private final RoomDetailsServicePort roomDetailsServicePort;
    private final RoomMapper roomMapper;

    @GetMapping("/api/room")
    public List<RoomDto> getRooms() {
        return roomDetailsServicePort.getRooms().stream().map(roomMapper::fromRoomDetailsToDto).toList();
    }

    @GetMapping("/api/room/{number}")
    public RoomDto getRoomDetails(@PathVariable String number){
        return roomMapper.fromRoomDetailsToDto(roomDetailsServicePort.getRoomById(number));
    }

    @ExceptionHandler(InvalidRoomNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public WswErrorDetails handleInvalidRoomNumberException(InvalidRoomNumberException ex) {
        log.error("Invalid room number");
        return WswErrorDetails.builder().code("6").message("Invalid Room Number").build();
    }

    @ExceptionHandler(RoomNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public WswErrorDetails handleRoomNotFoundException(RoomNotFoundException ex) {
        log.error("Room not found");
        return WswErrorDetails.builder().code("5").message("Room Not Found").build();
    }

}
