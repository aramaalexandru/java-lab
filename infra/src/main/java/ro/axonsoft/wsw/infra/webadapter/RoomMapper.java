package ro.axonsoft.wsw.infra.webadapter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.ReportingPolicy;
import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.infra.webmodel.RoomDto;

@Mapper(componentModel = ComponentModel.SPRING, uses = {PersonMapper.class}, unmappedTargetPolicy = ReportingPolicy.WARN)
public interface RoomMapper {

  @Mapping(source = "people", target = "persons")
  @Mapping(source = "room", target = "roomNumber")
  RoomDetails fromRoomDtoToDetails(RoomDto room);

  @Mapping(source = "persons", target = "people")
  @Mapping(source = "roomNumber", target = "room")
  RoomDto fromRoomDetailsToDto(RoomDetails room);
}
