package ro.axonsoft.wsw.infra.webadapter;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import ro.axonsoft.wsw.core.exception.WswErrorDetails;
import ro.axonsoft.wsw.core.exception.WswException;

@ControllerAdvice
public class RoomExceptionHandler {

  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public void handleMediaTypeNotSupportedException() {

   }

  @ExceptionHandler(WswException.class)
  public ResponseEntity<WswErrorDetails> handleWswException(WswException ex) {
    var errorCode = ex.getErrorCode();
    return ResponseEntity.status(HttpStatus.valueOf(errorCode.getHttpStatusCode()))
        .body(WswErrorDetails.builder()
            .code(errorCode.getCode())
            .message(errorCode.getMessage()).build());
  }


}
