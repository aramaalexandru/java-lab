package ro.axonsoft.wsw.infra.webmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PersonDto {
  String ldapuser;
  @JsonProperty("first name")
  String firstName;
  @JsonProperty("last name")
  String lastName;
  @JsonProperty("name addition")
  String nameAddition;
  String title;
}
