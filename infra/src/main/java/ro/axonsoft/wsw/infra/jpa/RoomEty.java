package ro.axonsoft.wsw.infra.jpa;

import java.util.List;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "ROOM")
@Data
@EqualsAndHashCode(of = "id")
public class RoomEty {

  @Id
  @Column(name = "ROOMNUMBER")
  public String id;

  @ElementCollection
  @JoinTable(name = "ROOM_PERSON", joinColumns = @JoinColumn(name = "ROOMNUMBER"))
  public List<PersonEty> people;
}
