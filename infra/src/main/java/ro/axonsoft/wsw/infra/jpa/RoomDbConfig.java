package ro.axonsoft.wsw.infra.jpa;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = "ro.axonsoft.wsw.infra.jpa")
@EnableJpaRepositories
public class RoomDbConfig {

}
