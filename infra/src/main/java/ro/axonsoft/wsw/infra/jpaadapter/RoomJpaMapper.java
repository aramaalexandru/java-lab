package ro.axonsoft.wsw.infra.jpaadapter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.ReportingPolicy;
import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.infra.jpa.RoomEty;

@Mapper(componentModel = ComponentModel.SPRING, uses = {PersonJpaMapper.class}, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface RoomJpaMapper {

  @Mapping(source = "id", target = "roomNumber")
  @Mapping(source = "people", target = "persons")
  RoomDetails fromRoomEtyToDetails(RoomEty room);

  @Mapping(source = "roomNumber", target = "id")
  @Mapping(source = "persons", target = "people")
  RoomEty fromRoomDetailsToEty(RoomDetails room);

}
