package ro.axonsoft.wsw.infra.webadapter;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.ReportingPolicy;
import ro.axonsoft.wsw.core.domain.PersonDetails;
import ro.axonsoft.wsw.infra.webmodel.PersonDto;

@Mapper(componentModel = ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.WARN)
public interface PersonMapper {

  PersonDto fromPersonDetailsToDto(PersonDetails person);

  PersonDetails fromPersonDtoToDetails(PersonDto person);
}
