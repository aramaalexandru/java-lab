package ro.axonsoft.wsw.infra;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ro.axonsoft.wsw.infra.jpa.RoomDbConfig;
import ro.axonsoft.wsw.infra.jpaadapter.RoomRepositoryAdapter;
import ro.axonsoft.wsw.infra.webadapter.RoomController;
import ro.axonsoft.wsw.infra.webadapter.RoomImportController;

@Configuration
@Import({RoomDbConfig.class, RoomRepositoryAdapter.class, RoomController.class,
    RoomImportController.class})
public class InfraAutoConfig {

}
