package ro.axonsoft.wsw.infra.webmodel;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RoomDto {
  String room;
  List<PersonDto> people;
}
