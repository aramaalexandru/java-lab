package ro.axonsoft.wsw.infra.jpaadapter;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.infra.jpa.RoomRepository;
import ro.axonsoft.wsw.core.domain.port.RoomRepoPort;

@Component
@RequiredArgsConstructor
public class RoomRepositoryAdapter implements RoomRepoPort {

  private final RoomRepository roomRepository;
  private final RoomJpaMapper roomMapper;

  @Transactional(readOnly = true)
  @Override
  public List<RoomDetails> getRooms() {
    return roomRepository.findAll().stream().map(roomMapper::fromRoomEtyToDetails).toList();
  }

  @Transactional(readOnly = true)
  @Override
  public Optional<RoomDetails> getRoomById(String id) {
    return roomRepository.findById(id).map(roomMapper::fromRoomEtyToDetails);
  }

  @Override
  public void saveAll(List<RoomDetails> rooms) {
    roomRepository.saveAll(rooms.stream().map(roomMapper::fromRoomDetailsToEty).toList());
  }
}
