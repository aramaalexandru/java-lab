package ro.axonsoft.wsw.infra.jpaadapter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants.ComponentModel;
import org.mapstruct.ReportingPolicy;
import ro.axonsoft.wsw.core.domain.PersonDetails;
import ro.axonsoft.wsw.infra.jpa.PersonEty;

@Mapper(componentModel = ComponentModel.SPRING, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface PersonJpaMapper {

  @Mapping(source = "id", target = "ldapuser")
  PersonDetails fromPersonEtyToDetails(PersonEty person);

  @Mapping(source = "ldapuser", target = "id")
  PersonEty fromPersonDetailsToEty(PersonDetails person);

}
