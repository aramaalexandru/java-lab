package ro.axonsoft.wsw.core.domain.port;

import java.util.List;
import java.util.Optional;
import ro.axonsoft.wsw.core.domain.RoomDetails;

public interface RoomRepoPort {
  List<RoomDetails> getRooms();
  Optional<RoomDetails> getRoomById(String id);
  void saveAll(List<RoomDetails> rooms);
}
