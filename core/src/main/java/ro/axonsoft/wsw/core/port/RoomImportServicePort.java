package ro.axonsoft.wsw.core.domain.port;


import java.io.InputStream;

public interface RoomImportServicePort {

    void uploadFile(InputStream file);
}
