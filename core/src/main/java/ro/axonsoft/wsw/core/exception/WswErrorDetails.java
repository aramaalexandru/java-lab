package ro.axonsoft.wsw.core.exception;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class WswErrorDetails {
    String code;
    String message;
}
