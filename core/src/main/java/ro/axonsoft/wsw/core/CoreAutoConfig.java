package ro.axonsoft.wsw.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ro.axonsoft.wsw.core.service.RoomDetailsService;
import ro.axonsoft.wsw.core.service.RoomImportService;

@Configuration
@Import({RoomDetailsService.class, RoomImportService.class})
public class CoreAutoConfig {

}
