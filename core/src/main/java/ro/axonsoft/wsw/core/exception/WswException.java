package ro.axonsoft.wsw.core.exception;

import lombok.Getter;

@Getter
public class WswException extends RuntimeException {

    private final WswErrorCode errorCode;

    public WswException(WswErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
