package ro.axonsoft.wsw.core.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum WswErrorCode {

    DUPLICATE_ROOM("2", "Duplicate room found in imported file!", 400),
    DUPLICATE_PERSON("3", "Duplicate person found in imported file!", 400),
    INVALID_LINE("4", "Invalid line found in imported file!", 400);

    private final String code;
    private final String message;
    private final int httpStatusCode;
}
