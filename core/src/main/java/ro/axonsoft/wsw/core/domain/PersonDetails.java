package ro.axonsoft.wsw.core.domain;

import lombok.*;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
public class PersonDetails {
  String ldapuser;
  String firstName;
  String lastName;
  String nameAddition;
  String title;
}
