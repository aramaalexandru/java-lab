package ro.axonsoft.wsw.core.service;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.core.domain.port.RoomRepoPort;
import ro.axonsoft.wsw.core.exception.InvalidRoomNumberException;
import ro.axonsoft.wsw.core.exception.RoomNotFoundException;
import ro.axonsoft.wsw.core.port.RoomDetailsServicePort;

@Service
@RequiredArgsConstructor
public class RoomDetailsService implements RoomDetailsServicePort {

    private final RoomRepoPort roomRepository;

    public List<RoomDetails> getRooms() {
        return roomRepository.getRooms();
    }

    public RoomDetails getRoomById(String id) {
        if (id == null || !id.matches("^[0-9]{4}$")) {
            throw new InvalidRoomNumberException();
        }
        return roomRepository.getRoomById(id).orElseThrow(RoomNotFoundException::new);
    }
}
