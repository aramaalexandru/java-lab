package ro.axonsoft.wsw.core.service;

import static java.util.Objects.nonNull;

import com.google.common.io.CharStreams;
import com.opencsv.CSVReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import ro.axonsoft.wsw.core.domain.PersonDetails;
import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.core.domain.port.RoomImportServicePort;
import ro.axonsoft.wsw.core.domain.port.RoomRepoPort;
import ro.axonsoft.wsw.core.exception.WswErrorCode;
import ro.axonsoft.wsw.core.exception.WswException;

@Service
@RequiredArgsConstructor
public class RoomImportService implements RoomImportServicePort {

    private final RoomRepoPort roomRepository;

    private static final Pattern PERSON_REGEX =
            Pattern.compile("(?<title>Dr\\.)?\\s?(?<firstname>[a-zA-Züöäß-]+)(\\s(?<middlename>(?!von|van|de)[a-zA-Züöäß-]+))?" +
                    "(\\s(?<affix>(van|von|de)))?\\s(?<lastname>[a-zA-Züöäß-]+)(\\s(?<ldapuser>\\([a-zA-Züöäß-]+\\)))");

    @SneakyThrows
    @Override
    public void uploadFile(InputStream file) {
        try (Reader reader = new InputStreamReader(file)) {
            var content = CharStreams.toString(reader);
            var rooms = parseRooms(content);
            handleDuplicateRooms(rooms);
            handleDuplicatePeople(rooms);
            roomRepository.saveAll(rooms);
        }
    }

    private void handleDuplicateRooms(List<RoomDetails> rooms) {
        rooms.stream().collect(Collectors.toMap(RoomDetails::getRoomNumber, roomDetails -> roomDetails, (room1, room2) -> {
            throw new WswException(WswErrorCode.DUPLICATE_ROOM);
        }));
    }

    private void handleDuplicatePeople(List<RoomDetails> rooms) {
        rooms.stream().flatMap(room -> room.getPersons().stream())
                .collect(Collectors.toMap(PersonDetails::getLdapuser, personDetails -> personDetails, (person1, person2) -> {
                    throw new WswException(WswErrorCode.DUPLICATE_PERSON);
                }));
    }

    @SneakyThrows
    private List<RoomDetails> parseRooms(String content) {
        var rooms = new ArrayList<RoomDetails>();
        try (Reader reader = new StringReader(content)) {
            try (CSVReader csvReader = new CSVReader(reader)) {
                String[] line;
                while (nonNull(line = csvReader.readNext())) {
                    rooms.add(RoomDetails.builder()
                        .roomNumber(line[0])
                        .persons(Arrays.stream(line).skip(1).filter(s -> !s.isEmpty())
                            .map(this::parsePerson).toList())
                        .build());
                }
            }
        }
        return rooms;
    }

    private PersonDetails parsePerson(String s) {
        var matcher = PERSON_REGEX.matcher(s);
        if (matcher.matches()) {
            return PersonDetails.builder()
                .ldapuser(matcher.group("ldapuser").replaceAll("[\\(\\)]", "").trim())
                .firstName(Strings.join(Arrays.asList(matcher.group("firstname"), matcher.group("middlename")), ' ').trim())
                .lastName(matcher.group("lastname").trim())
                .title(Optional.ofNullable(matcher.group("title")).orElse("").trim())
                .nameAddition(Optional.ofNullable(matcher.group("affix")).orElse("").trim())
                .build();
        } else throw new WswException(WswErrorCode.INVALID_LINE);
    }
}
