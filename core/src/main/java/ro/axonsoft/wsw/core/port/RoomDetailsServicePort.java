package ro.axonsoft.wsw.core.port;

import java.util.List;
import ro.axonsoft.wsw.core.domain.RoomDetails;

public interface RoomDetailsServicePort {
  List<RoomDetails> getRooms();
  RoomDetails getRoomById(String id);
}
