package ro.axonsoft.wsw.core.domain;

import java.util.List;

import lombok.*;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
public class RoomDetails {
  String roomNumber;
  List<PersonDetails> persons;
}
