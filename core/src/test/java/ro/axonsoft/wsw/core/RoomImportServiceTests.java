package ro.axonsoft.wsw.core;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.CharStreams;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.core.domain.port.RoomRepoPort;
import ro.axonsoft.wsw.core.exception.WswException;
import ro.axonsoft.wsw.core.service.RoomImportService;

@ExtendWith(MockitoExtension.class)
class RoomImportServiceTests {

    @Mock
    RoomRepoPort roomRepoPort;

    ObjectMapper objectMapper = new ObjectMapper();

    @InjectMocks
    RoomImportService roomImportService;

    String readFile(String fileName) throws IOException {
        try (Reader reader = new InputStreamReader(
                checkNotNull(RoomImportServiceTests.class.getClassLoader().getResourceAsStream(fileName),
                        "File %s Not Found",
                        fileName),
                StandardCharsets.UTF_8)) {
            return CharStreams.toString(reader);
        }
    }

    @Test
    void uploadFile_withValidData() throws IOException {
        var data = objectMapper.readValue(readFile("rooms_valid.json"), RoomDetails.class);
        roomImportService.uploadFile(
                RoomImportServiceTests.class.getClassLoader().getResourceAsStream("sitzplan_valid.csv"));
        verify(roomRepoPort).saveAll(List.of(data));
    }

    @Test
    void uploadFile_withInvalidLine() {
        var exception = assertThrows(WswException.class, () -> roomImportService.uploadFile(
                RoomImportServiceTests.class.getClassLoader().getResourceAsStream("sitzplan_invalid.csv")));
        verifyNoInteractions(roomRepoPort);
        assertThat(exception.getErrorCode().getCode()).isEqualTo("4");
    }

    @Test
    void uploadFile_withDuplicateRoom() {
        var exception = assertThrows(WswException.class, () -> roomImportService.uploadFile(
                RoomImportServiceTests.class.getClassLoader().getResourceAsStream("sitzplan_duproom.csv")));
        verifyNoInteractions(roomRepoPort);
        assertThat(exception.getErrorCode().getCode()).isEqualTo("2");
    }

    @Test
    void uploadFile_withDuplicatePerson() {
        var exception = assertThrows(WswException.class, () -> roomImportService.uploadFile(
                RoomImportServiceTests.class.getClassLoader().getResourceAsStream("sitzplan_dupperson.csv")));
        verifyNoInteractions(roomRepoPort);
        assertThat(exception.getErrorCode().getCode()).isEqualTo("3");
    }
}
