package ro.axonsoft.wsw.core;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ro.axonsoft.wsw.core.domain.PersonDetails;
import ro.axonsoft.wsw.core.domain.RoomDetails;
import ro.axonsoft.wsw.core.domain.port.RoomRepoPort;

import java.util.List;
import java.util.Optional;
import ro.axonsoft.wsw.core.exception.InvalidRoomNumberException;
import ro.axonsoft.wsw.core.exception.RoomNotFoundException;
import ro.axonsoft.wsw.core.service.RoomDetailsService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoomDetailsServiceTest {

    @Mock
    private RoomRepoPort roomRepoPort;

    @InjectMocks
    private RoomDetailsService roomDetailsService;

    @Test
    void whenCallGetRooms_thenDelegatesToRepo() {
        var rooms = List.of();
        doReturn(rooms).when(roomRepoPort).getRooms();
        roomDetailsService.getRooms();
        verify(roomRepoPort).getRooms();
    }

    @Test
    void whenCallGetRooms_thenReturnAllRooms() {
        var room1 = new RoomDetails();
        room1.setRoomNumber("2222");
        room1.setPersons(List.of());
        var room2 = new RoomDetails();
        room2.setRoomNumber("2223");
        var person = new PersonDetails();
        person.setFirstName("Frank");
        person.setLastName("Super");
        person.setLdapuser("dsfs");
        person.setNameAddition("van");
        person.setTitle("");
        room2.setPersons(List.of(person));

        doReturn(List.of(room1, room2)).when(roomRepoPort).getRooms();
        assertThat(roomDetailsService.getRooms()).satisfies(rooms -> {
            assertThat(rooms).hasSize(2);
            assertThat(rooms.get(0).getRoomNumber()).isEqualTo("2222");
            assertThat(rooms.get(0).getPersons()).isEmpty();
            assertThat(rooms.get(1).getRoomNumber()).isEqualTo("2223");
            assertThat(rooms.get(1).getPersons()).hasSize(1);
            assertThat(rooms.get(1).getPersons().get(0).getLdapuser()).isEqualTo("dsfs");
            assertThat(rooms.get(1).getPersons().get(0).getTitle()).isEmpty();
        });
        verify(roomRepoPort).getRooms();
    }

    @Test
    void whenCallGetRoomById_thenDelegatesToRepo() {
        var room = new RoomDetails();
        room.setRoomNumber("1111");
        room.setPersons(List.of());
        doReturn(Optional.of(room)).when(roomRepoPort).getRoomById("1111");
        roomDetailsService.getRoomById("1111");
        verify(roomRepoPort).getRoomById("1111");
    }

    @Test
    void whenCallGetRoomById_thenReturnRoom() {
        var room = new RoomDetails();
        room.setRoomNumber("1111");
        var person = new PersonDetails();
        person.setFirstName("Frank");
        person.setLastName("Super");
        person.setLdapuser("dsfs");
        person.setNameAddition("van");
        person.setTitle("");
        room.setPersons(List.of(person));
        doReturn(Optional.of(room)).when(roomRepoPort).getRoomById("1111");
        assertThat(roomDetailsService.getRoomById("1111")).satisfies(room1 -> {
            assertThat(room1.getRoomNumber()).isEqualTo("1111");
            assertThat(room1.getPersons().get(0).getLdapuser()).isEqualTo("dsfs");
            assertThat(room1.getPersons().get(0).getTitle()).isEmpty();
        });
        verify(roomRepoPort).getRoomById("1111");
    }

    @Test
    void whenCallGetUnknownRoomById_thenGetRoomNotFound() {
        doReturn(Optional.empty()).when(roomRepoPort).getRoomById("9999");
        assertThatCode(() -> roomDetailsService.getRoomById("9999")).isInstanceOf(
            RoomNotFoundException.class);
        verify(roomRepoPort).getRoomById("9999");
    }

    @Test
    void whenInvalidCharactersRoomNumber_thenThrowException() {
        assertThatCode(() -> roomDetailsService.getRoomById("abcd")).isInstanceOf(
            InvalidRoomNumberException.class);
        verifyNoInteractions(roomRepoPort);
    }

    @Test
    void whenInvalidAboveLengthLimRoomNumber_thenThrowException() {
        assertThatCode(() -> roomDetailsService.getRoomById("56789")).isInstanceOf(InvalidRoomNumberException.class);
        verifyNoInteractions(roomRepoPort);
    }

    @Test
    void whenInvalidBellowLengthLimRoomNumber_thenThrowException() {
        assertThatCode(() -> roomDetailsService.getRoomById("567")).isInstanceOf(InvalidRoomNumberException.class);
        verifyNoInteractions(roomRepoPort);
    }

    @Test
    void whenInvalidNullRoomNumber_thenThrowException() {
        assertThatCode(() -> roomDetailsService.getRoomById(null)).isInstanceOf(InvalidRoomNumberException.class);
        verifyNoInteractions(roomRepoPort);
    }

}
